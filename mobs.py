import random,time
from playerskills import playerskill



def randomname():
        philosopherllist = ['Schopenhauer','Nietzsche','Erythroporo','Plato']
        randomint = random.choice(philosopherllist)
        return randomint
#testi

class player():
        def __init__(self, hp, damage, mana):
            self.hp = hp
            self.damage = damage
            self.mana = mana
            self.name = 'Aki'
            self.exp = 0
            self.skilllist = ['Rationalization', 'Ad Hominem']
            skills = playerskill()
        def Rationalization(self):
            if 'Rationalization' in self.skilllist:
                damage = random.randint(0,self.damage)
            return damage

        def get__exp(self):
            return self.exp
       
        def AdHominem(self):
            if 'Ad Hominem' in self.skilllist:
                damage = random.randint(0,2*self.damage)
                print("===================================")
                print("Your face is ugly")
            return damage

        def givestats(self,hpz, manaz, exp):
            self.hp += hpz
            self.mana += manaz
            self.exp += exp


        def take_damage(self,damage):
            if self.hp == 0 or self.hp < 0:
                print("{0} withdraws from the argument in agony, his philosophy humiliated".format(self.name))
                return 0
            self.hp = self.hp - damage
            print("Player HP:{0} Player Mana:{1}".format(self.hp,self.mana))

        def attackchooser(self, choice):
            skills = playerskill()
            if choice == 1:
                damage = self.Rationalization()
                return damage
            elif choice == 2:
                damage = self.AdHominem()
                return damage
            elif choice == 3 and skills.get__stonegazename() in self.skilllist:
                damage = skills.StoneGaze(self)
                return damage
            else:
                damage = 0
                return damage
        def update__skilllist(self):
            skills = playerskill()
            if skills.get__stonegazeexprecuirement() < self.get__exp() and skills.get__stonegazename() not in self.skilllist:
                self.skilllist.append(skills.get__stonegazename())


class monster():
    'Common class for all the monsters in the game'

    def __init__(self, hp, damage, mana):
        self.hp = hp
        self.damage = damage
        self.mana = mana
        self.loot = 30
        self.name = 'Monster'
    def take_damage(self,damage):
        loots = []
        self.hp -=  damage
        print("{0} HP: {1}".format(self.name, self.hp))
        if self.hp == 0 or self.hp < 0:
            print("{0} withdraws from the argument in agony, his philosophy humiliated".format(self.name))
            for i in range(0,3):
                loots.append(random.randint(1,30))
            return loots

    def armorreduction(self,damage,armorlevel):
        if armorlevel == 1:
            returneddamage = damage * 0.9
        elif armorlevel == 2:
            returneddamage = damage * 0.7
        elif armorlevel == 3:
            returneddamage = damage * 0.5

    def Rationalization(self):
        damage = random.randint(0,self.damage)
        return damage

    def mana_check(self, amount):
        if self.mana < amount:
            print("{0} is out of mana so the attack fails!".format(self.name))
class Plato(monster):
    def __init__(self,hp,mana,damage):
        super().__init__(hp,mana,damage)
        self.name = 'Plato'
    def mechanix(self,playerhealth):
        if playerhealth > self.damage or self.mana > 20:
            damage =self.Essentialism()
            return damage
        else:
            damage = self.Rationalization()
            self.mana+=10
            print("Plato recovers 10 credibility!")
            return damage
    def Essentialism(self):    #Have to invent something creative for this one
        if self.mana < 10:
            print("Plato is out of credibility so his attack fails")
            self.mana += 10
            print("Plato recovers 10 credibility!")
            print("And hits you with Rationalization!")
            damage = self.Rationalization()
            return damage
        else:
            print("Plato hits you with Human Essentialism")
            damage = self.Rationalization()
            damage = damage * 2
            if damage == 0:
                 print("But your mind is strong enough to point out the logical flaws in his argument")
            self.mana -= 20
            return damage


class Schopenhauer(monster):
    def __init__(self,hp,mana,damage):
        print("Willpower is to the mind like a strong blind man who carries on his shoulders a lame man who can see.")
        self.name = 'Schopenhauer'
        self.hp = hp
        self.mana = mana
        self.damage = damage



    def mechanix(self,playerhealth):
        if playerhealth > self.damage or self.mana > 20:
            damage =self.CrisisOfExistentialism()
            return damage
        elif playerhealth <= 5:
            damage = self.Rationalization()
            self.mana+=10
            print("Schopenhauer recovers 10 credibility!")
            return damage
    def CrisisOfExistentialism(self):
            print("Schopenhauer hits you with the Crisis of Existentialism")
            damage = self.Rationalization()
            damage = damage * 2
            if damage == 0:
                print("But your mind is strong enough to resist the foul impulses of his sick misanthropic philosophy")
            self.mana -= 20
            return damage
class Nietzsche(monster):
    def __init__(self,hp,mana,damage):
        print("God is dead.")
        self.name = 'Nietzsche'
        self.hp = hp
        self.mana = mana
        self.damage = damage


    def mechanix(self,playerhealth):
        if playerhealth > self.damage or self.mana > 20:
            damage =self.CrisisOfExistentialism()
            return damage
        else:
            damage = self.Rationalization()
            self.mana+=10
            print("Nietzsche recovers 10 credibility!")
            return damage
    def CrisisOfExistentialism(self):
        if self.mana < 10:
            print("Nietzsche is out of credibility so his attack fails")
            self.mana += 10
            print("Nietzsche recovers 10 credibility!")
            return 0
        else:
            print("Nietzsche hits you with Ubermensch")
            damage = self.Rationalization()
            damage = damage * 10
            if damage == 0:
                print("But your mind is strong enough to resist the foul impulses of his sick misanthropic philosophy")
            self.mana -= 20
            return damage


class Erythroporo(monster):
    def __init__(self,hp,mana,damage):
        print("Gods are the source of all good, therefore morality from gods has to be good.")
        self.name = 'Erythroporo'
        self.hp = hp
        self.mana = mana
        self.damage = damage

    def mechanix(self,playerhealth):
        if playerhealth > self.damage or self.mana > 20:
            damage =self.moralitycomesfromgods()
            return damage
        else:
            damage = self.Rationalization()
            self.mana+=10
            print("Erythroporo recovers 10 credibility!")
            return damage
    def moralitycomesfromgods(self):
        if self.mana < 10:
            print("Erythroporo is out of credibility so his attack fails")
            self.mana += 10
            print("Erythroporo recovers 10 credibility!")
            return 0
        else:
            print("Erythroporo hits you with the Morality Comes from The Gods")
            damage = self.Rationalization()
            damage = damage * 1
            if damage == 0:
                print("But your mind is strong enough to resist the foul impulses of his sick misanthropic philosophy")
            self.mana -= 20
            return damage
