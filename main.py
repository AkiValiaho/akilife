import winsound,mobs,mobgenerator,os,time


def main():
    winsound.PlaySound('crockett.wav', winsound.SND_ASYNC) #Playing Crockett's theme asynchronously, not included here
    player = mobs.player(100, 20, 100)
    skillnumber = 1
    try:
        while True:
            print("===================================")
            print("A wild philosopher arrives:")
            philosopherclass = mobgenerator.spawner()
            print("ITS  {0}!".format(philosopherclass.name.upper()))
            print("===================================")
            while True:
                player.take_damage(philosopherclass.mechanix(player.hp))
                if player.hp == 0 or player.hp <0:
                    print("You have lost, but you will fight to live another day!")
                print("===========================================")
                print("How do you plan on defending yourself?")
                print("You can currently attack with these attacks: ",end='')
                player.update__skilllist()
                for i in player.skilllist:
                    print(str(skillnumber) +'.'+i)
                    skillnumber += 1
                print("===========================================")
                skillnumber = 1
                sense = False
                while sense == False:
                    try:
                        choice = int(input("Make your choice by inputting the number specified in the skill list"))
                        os.system('cls')
                        loots=philosopherclass.take_damage(player.attackchooser(choice))
                        if loots != None:
                            player.givestats(loots[0],loots[1],loots[2])
                            print("=========================================================")
                            print("The death of the great philosopher has awarded you loots!")
                            print("You now have {0} hp {1} mana and {2} exp".format(player.hp,player.mana,player.exp))
                            print("========================================")
                            time.sleep(5)
                            os.system('cls')
                        sense = True
                    except ValueError:
                        print("Your choice didn't make a sense, please try again")
                if loots != None:
                    break

    except IOError:
            print("THE  GAME IS CORRUPTED!")
main()