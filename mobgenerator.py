
import mobs,random
def spawner():
    hp = random.randint(1,100)
    mana = random.randint(1,100)
    damage = random.randint(1,10)
    name=mobs.randomname()
    if name == 'Schopenhauer':
       mobclass = mobs.Schopenhauer(hp,mana,damage)

    elif name == 'Nietzsche':
        mobclass = mobs.Nietzsche(hp,mana,damage)


    elif name == 'Erythroporo':
        mobclass = mobs.Erythroporo(hp,mana,damage)

    elif name == 'Plato':
        mobclass = mobs.Plato(hp,mana,damage)
    return mobclass

